# Violent Thruth by Mythic Toybox

PLAY HERE: https://play.unity.com/mg/other/webgl-builds-389149  

This project is my semestral submission for the subject BI-VHS 2023 winter semester.  
Created by:  
    -   Benedek Molnár
    -   Martin Salaj
    -   Matěj Doležal
    -   Matej Stieranka
    -   Matěj Vlček
    -   Tadeáš Hejnic



### Introduction:  

Embark on a pixelated odyssey in our 2D top-down open-world adventure! Battle the forces of blandness as you deliver joy through beer in a quest to restore happiness to a world tainted by the "non-alcoholic" menace. Engage in epic battles, solve quirky puzzles, and explore a visually stunning pixel art universe. Become the legendary Brewmaster, pouring happiness into every pixel and reclaiming the realm from the shadows of sobriety. Are you ready to clink and cheer your way to pixelated victory? Cheers to a beertastic journey!


# Concept

**PLEASE SEE THE GAME DESIGN DOCUMENT**

## Getting Started

1.  **Clone the Repository:**
    
    Copy code
    
    `git clone https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/mythic_toybox` 
    
2.  **Open in Unity:**
    
    -   Launch Unity Hub.
    -   Click on "Add" and select the cloned repository's folder.
3.  **Set Up Unity Project:**
    
    -   Open the project in Unity.
    -   If prompted, upgrade the project to the Unity version specified in the repository.
4.  **Run the Game:**
    
    -   Navigate to the main scene.
    -   Press the play button to run the game.


## Building the Unity Game

1.  **Open the Unity Project:**
    
    -   Launch Unity Hub.
    -   Open the Unity project you want to build.

2.  **Configure Build Settings:**
    
    -   Go to `File` > `Build Settings` in the Unity editor.
    -   Select the scenes you want to include in the build.

3.  **Platform Selection:**
    
    -   Choose your target platform (e.g., PC, Mac, Linux) using the "Platform" dropdown.

4.  **Build Settings:**
    
    -   Configure build settings such as build location and options.
    -   Click on "Build" or "Build and Run" to start the build process.

5.  **Output Directory:**
    
    -   Specify a folder where you want to save the built executable.

6.  **Wait for Completion:**
    
    -   Unity will compile and build the project. Wait for the process to complete.

7.  **Run the Executable:**
    
    -   Once the build is finished, navigate to the output directory.
    -   Run the generated executable to play the standalone version of your game.



## Open License for School Project:

This project, titled Violent Truth, is created and owned by the team Mythic Toybox. As part of an educational initiative, I hereby grant the following permissions for academic use within the scope of school-related activities:

1.  **Usage:** You are allowed to use, modify, and distribute this game for educational purposes, including classroom projects, presentations, and demonstrations.
    
2.  **Attribution:** When using or presenting this project, kindly attribute the original work to the team Mythic Toybox.
    
3.  **Non-Commercial:** This project and its assets may not be used for commercial purposes without explicit permission from the creator.
    
4.  **Derivative Works:** You are encouraged to create derivative works based on this project for educational purposes, giving appropriate credit.
    
5.  **Distribution:** The distribution of this project or its modified versions should be limited to educational platforms and not for commercial gain.
    

By adhering to these guidelines, users can enjoy the educational benefits of Violent Truth while respecting the creator's intellectual property rights. 31.1.2024
