using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodSpawner : MonoBehaviour
{
    public GameObject woodPrefab;

    public Transform spawnPosition;

    public Player.Item itemToSpawn = Player.Item.wood;

    private Object spawnedInstance = null;

    private bool spawningLock = false;

    private WorldLogic worldLogic;

    private void Start()
    {
        worldLogic = FindObjectOfType<WorldLogic>();
    }

    private void Update()
    {

        if (spawnedInstance == null && !spawningLock) 
        {
            spawningLock = true;
            StartCoroutine(SpawnItemCoroutine());
        }
    }

    private IEnumerator SpawnItemCoroutine() 
    {
        yield return new WaitForSeconds(Random.Range(1f, 5f));
        spawnedInstance = Instantiate(woodPrefab, spawnPosition);
        spawningLock = false;
    }

    public void ResetSpawner() 
    {
        spawnedInstance = null;
    }
}
