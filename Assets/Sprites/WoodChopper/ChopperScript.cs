using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChopperScript : MonoBehaviour
{
    Animator animator;
    public float offset = 1;
    
    public DetectionZoneChopper detectionZone;
    Rigidbody2D rb;
    public WorldLogic world;

    void Start(){
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate(){
        

        if(detectionZone.detectedObjs.Count != 0){
            animator.SetTrigger("isAnimal"); 
        }

        animator.SetBool("isNight", world.isNight);
    }
}
