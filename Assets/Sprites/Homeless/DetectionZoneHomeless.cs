using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionZoneHomeless : MonoBehaviour
{
    public List<Collider2D> detectedObjs = new List<Collider2D>();
    public Collider2D col;
    // Start is called before the first frame update
    void Start()
    {
        //col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider){
        
        if(collider.gameObject.tag == "Animal"){
            detectedObjs.Add(collider);
        }
    }

    void OnTriggerExit2D(Collider2D collider){
        
        if(collider.gameObject.tag == "Animal"){
            detectedObjs.Remove(collider);
        }
        
    }
}
