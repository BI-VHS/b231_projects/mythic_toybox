using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomelessScript : MonoBehaviour
{
    Animator animator;
    public float offset = 1;
    
    public DetectionZoneHomeless detectionZone;
    Rigidbody2D rb;
    private WorldLogic world;

    void Start(){
        world = FindObjectOfType<WorldLogic>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate(){
        

        if(detectionZone.detectedObjs.Count != 0){
            animator.SetTrigger("isAnimal"); 
            Collider2D detectedObject = detectionZone.detectedObjs[0];
            detectionZone.detectedObjs.Remove(detectedObject);
            detectedObject.gameObject.SetActive(false);
        }

        animator.SetBool("isNight", world.isNight);
    }
}
