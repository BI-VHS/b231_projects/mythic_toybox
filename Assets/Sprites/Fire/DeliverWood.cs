using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverWood : MonoBehaviour
{
    public bool wantsWood = false;

    public void GiveWood() 
    {
        gameObject.GetComponent<Animator>().SetTrigger("onFire");
        FindObjectOfType<Player>().heldItem = Player.Item.none;
    }
}
