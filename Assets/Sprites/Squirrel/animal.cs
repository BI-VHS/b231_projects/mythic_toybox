using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float moveSpeed = 500f;
    Animator animator;
    public float offset = 1;
    public bool shouldrun = false;
    
    public DetectionZone detectionZone;
    Rigidbody2D rb;

    public bool towards = false;

    void Start(){
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate(){

        int negator = -1;

        if (towards)
            negator = 1;
        

        if(detectionZone.detectedObjs.Count != 0){

            Collider2D detectedObject = detectionZone.detectedObjs[0];

            float distance = Vector2.Distance(detectedObject.transform.position,transform.position);

            if( distance < offset ){
                shouldrun = !shouldrun;
            }

            if( ! shouldrun ){
                return;
            }

            Vector2 direction = (detectedObject.transform.position - transform.position).normalized;

            rb.MovePosition(rb.position + direction * moveSpeed * Time.fixedDeltaTime * negator);
            animator.SetBool("isMoving", true);
            
        }
        else{
            animator.SetBool("isMoving", false);
            shouldrun=!shouldrun;
        }
    }
}
