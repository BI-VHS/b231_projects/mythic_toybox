using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkFilter : MonoBehaviour
{
    public bool toggled;
    void Update()
    {
        if (toggled)
        {
            gameObject.SetActive(true);
        }
        else { gameObject.SetActive(false); }
    }
}
