using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkingMan : MonoBehaviour
{
    public Animator animator;
    private bool isPlayerClose = false;

    void Start()
    {
        // Get the Animator component attached to the GameObject
        //animator = GetComponent<Animator>();
        
    }

    // OnTriggerEnter is called when the Collider other enters the trigger
    // OnTriggerEnter2D is called when the Collider2D other enters the trigger
    void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the colliding object has the tag "Player"
        if (other.CompareTag("Player"))
        {
            // Set the parameter isPlayerClose to true
            isPlayerClose = true;

            // Set the animation parameter in the Animator to trigger the working animation
            animator.SetBool("isPlayerClose", isPlayerClose);
        }
    }

    // OnTriggerExit2D is called when the Collider2D other exits the trigger
    void OnTriggerExit2D(Collider2D other)
    {
        // Check if the colliding object has the tag "Player"
        if (other.CompareTag("Player"))
        {
            // Set the parameter isPlayerClose to false
            isPlayerClose = false;

            // Set the animation parameter in the Animator to trigger the idle animation
            animator.SetBool("isPlayerClose", isPlayerClose);
        }
    }
}
