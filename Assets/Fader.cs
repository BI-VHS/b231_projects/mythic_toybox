using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    private Image image;
    public float fadeDuration = 0.3f;

    void Start()
    {
        image = gameObject.GetComponent<Image>();
        StartCoroutine(FadeOut());
    }

    public IEnumerator FadeIn()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);

        float currentAlpha = 0f;
        while (currentAlpha < 1f)
        {
            currentAlpha += 0.1f;
            image.color = new Color(image.color.r, image.color.g, image.color.b, currentAlpha);
            yield return new WaitForSeconds(fadeDuration);
        }
    }

    public IEnumerator FadeOut()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);

        float currentAlpha = 1f;
        while (currentAlpha > 0f)
        {
            currentAlpha -= 0.1f;
            image.color = new Color(image.color.r, image.color.g, image.color.b, currentAlpha);
            yield return new WaitForSeconds(fadeDuration);
        }
    }

    public IEnumerator FadeInFadeOut() 
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);

        float currentAlpha = 0f;
        while (currentAlpha < 1f)
        {
            currentAlpha += 0.1f;
            image.color = new Color(image.color.r, image.color.g, image.color.b, currentAlpha);
            yield return new WaitForSeconds(fadeDuration);
        }

        image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);

        currentAlpha = 1f;
        while (currentAlpha > 0f)
        {
            currentAlpha -= 0.1f;
            image.color = new Color(image.color.r, image.color.g, image.color.b, currentAlpha);
            yield return new WaitForSeconds(fadeDuration);
        }
    }
}
