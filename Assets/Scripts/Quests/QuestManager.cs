using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    public Text[] questSlots;
    public List<Quest> quests;
    public float waitAfterCross = 2f;

    public HashSet<string> finishedQuests = new HashSet<string>();

    public int beersDelivered = 0;

    public AudioSource addQuest;
    public AudioSource finishQuest;

    private bool questPrintLock = false;

    void Start()
    {
        ClearQuestLog();
        PrintQuests();
    }

    void ClearQuestLog()
    {
        foreach (Text text in questSlots)
        {
            if (text)
                text.text = string.Empty;
        }
    }

    public void PrintQuests()
    {
        while (questPrintLock) { }
        questPrintLock = true;
        for (int i = 0; i < questSlots.Length; i++)
        {
            bool deliverFlag = false;
            questSlots[i].color = Color.black;
            if (quests.Count > i)
            {
                if (quests[i].questId == "deliverbeer")
                    deliverFlag = true;
                questSlots[i].text = "- ";
                StartCoroutine(ScrollText(questSlots[i], quests[i].questText, deliverFlag));
            }
            else 
            {
                questSlots[i].text = string.Empty;
            }
        }
        questPrintLock = false;
    }

    private IEnumerator ScrollText(Text textSlot, string text, bool deliverFlag = false)
    {
        if (deliverFlag && beersDelivered < 5)
        {
            text += " [";
            text += beersDelivered.ToString();
            text += "/5]";
        }

        foreach (char c in text)
        {
            textSlot.text += c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void AddQuest(Quest quest) 
    {
        quests.Add(quest);
        PrintQuests();
        if(!addQuest.isPlaying) addQuest.Play();
    }

    public void FinishQuest(string questId) 
    {
        StartCoroutine(CrossOutQuest(questId));
        if(!finishQuest.isPlaying) finishQuest.Play();
    }

    private IEnumerator CrossOutQuest(string questId) 
    {
        foreach (Quest quest in quests)
        {
            if (quest.questId == questId)
            {
                finishedQuests.Add(questId);
                DisplayCrossDecor(quest);
                yield return new WaitForSeconds(waitAfterCross);
                quests.Remove(quest);
                PrintQuests();
                break;
            }
        }
    }

    private void DisplayCrossDecor(Quest quest) 
    {
        foreach (Text textField in questSlots) 
        {
            if (textField.text.Length > 0) 
            {
                string tmp = textField.text[2..];
                if (tmp == quest.questText)
                {
                    textField.color = new Color(8, 93, 0);
                    break;
                }
            }
        }
    }
}
