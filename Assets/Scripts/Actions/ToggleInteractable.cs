using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInteractable : Action
{
    public string objectName;
    public bool setTo = true;
    public override bool Execute()
    {
        if (objectName != null && GameObject.Find(objectName) != null)
        {
            Interactable interactable = GameObject.Find(objectName).GetComponent<Interactable>();
            if (interactable != null)
            {
                interactable.canInteract = setTo;
            }
        }
        return true;
    }
}
