using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishQuestIfFound : Action
{   
    public string questId;

    public override bool Execute()
    {
        if (questId != null)
            FindObjectOfType<QuestManager>().FinishQuest(questId);

        return true;
    }
}
