using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerQuest : Action
{
    public Quest questToTrigger;
    public override bool Execute()
    {
        FindAnyObjectByType<QuestManager>().AddQuest(questToTrigger);
        return true;
    }
}
