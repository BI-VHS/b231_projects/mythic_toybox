using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : Action
{
    public Player.Item item = Player.Item.beer;
    public override bool Execute()
    {
        FindObjectOfType<Player>().heldItem = item;
        return true;
    }
}
