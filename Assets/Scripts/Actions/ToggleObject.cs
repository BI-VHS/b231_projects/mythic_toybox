using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleObject : Action
{
    public string objectName;
    public override bool Execute()
    { 
        if (objectName != null && GameObject.Find(objectName) != null)
        {
            GameObject foundObject = GameObject.Find(objectName);
            foundObject.SetActive(!(foundObject.activeSelf));
        }
        return true;
    }
}
