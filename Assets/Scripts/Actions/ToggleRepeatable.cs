using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleRepeatable : Action
{
    public string objectName;
    public override bool Execute()
    {
        if (objectName != null && GameObject.Find(objectName) != null)
        {
            Interactable interactable = GameObject.Find(objectName).GetComponent<Interactable>();
            if (interactable != null)
            {
                interactable.repeatable = !interactable.repeatable;
                interactable.canInteract = !interactable.canInteract;
            }
        }
        return true;
    }
}
