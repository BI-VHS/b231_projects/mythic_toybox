using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleActions : Action
{   
    public List<Action> actions;
    public override bool Execute()
    {
        foreach (var action in actions)
        {
            action.Execute();
        }
        return true;
    }
}
