using UnityEngine;

[AddComponentMenu("Action/TriggerExampleDialogue")]
public class TriggerExampleDialogue : Action
{
    public GameObject dialogueToInvoke;
    public Action doAfter = null;
    public override bool Execute()
    {
        Object o = Instantiate(dialogueToInvoke);

        if (doAfter != null)
            doAfter.Execute();

        return true;
    }
}
