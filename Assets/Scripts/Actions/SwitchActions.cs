using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchActions : Action
{
    public string interactableName;
    public GameObject actionToInject;
    public override bool Execute()
    {
        if (interactableName != null && actionToInject != null) 
        {
            GameObject foundObject = GameObject.Find(interactableName);
            if (foundObject != null)
            {
                foundObject.GetComponent<Interactable>().interactionAction = actionToInject.GetComponent<Action>();
            }
        }
        return true;
    }
}
