using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncrementDeliveries : Action
{
    public enum BeerDrinker
    {
        farmer, grandma, officer, schoolboy, clerk
    }
    public BeerDrinker beerDrinker;
    public Action finalAction;

    public override bool Execute()
    {
        QuestManager questManager = FindObjectOfType<QuestManager>();
        questManager.beersDelivered += 1;
        questManager.PrintQuests();

        WorldLogic worldLogic = FindObjectOfType<WorldLogic>();
        switch (beerDrinker) 
        {
            case BeerDrinker.farmer: worldLogic.finishedFarm(); break;
            case BeerDrinker.grandma: worldLogic.finishedGrandma(); break;
            case BeerDrinker.officer: worldLogic.finishedComrade();  break;
            case BeerDrinker.schoolboy: worldLogic.finishedSchool(); break;
            case BeerDrinker.clerk: worldLogic.finishedGrocery(); break;
        }

        if (questManager.beersDelivered >= 5) 
        {
            worldLogic.finishedAll();
            questManager.FinishQuest("deliverbeer");
            GameObject.Find("TriggerEnd").GetComponent<TriggerEndDialogue>().Execute();
        }

        return true;
    }
}
