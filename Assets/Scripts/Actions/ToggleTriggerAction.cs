using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTriggerAction : Action
{
    public string triggerToToggleName;
    public List<string> questRequirements;

    public override bool Execute()
    {
        if (questRequirements.Count > 0) 
        {
            foreach (var requirement in questRequirements)
            {
                if (!(FindObjectOfType<QuestManager>().finishedQuests.Contains(requirement)))
                    return true;
            }
        }

        if (triggerToToggleName != null && GameObject.Find(triggerToToggleName) != null && !FindObjectOfType<WorldLogic>().buddyTriggerAlreadyActivated) 
        {
            FindObjectOfType<WorldLogic>().buddyTriggerAlreadyActivated = true;
            Trigger toToggle = GameObject.Find(triggerToToggleName)?.GetComponent<Trigger>();
            toToggle.triggerEnabled = !toToggle.triggerEnabled;
        }
        return true;
    }
}
