using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenChurch : Action
{
    public override bool Execute()
    {
        GameObject.Find("InteractTransportChurch").GetComponent<Interactable>().openAtNight = true;
        return true;
    }
}
