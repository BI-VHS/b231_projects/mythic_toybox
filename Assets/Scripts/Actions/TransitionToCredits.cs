using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionToCredits : Action
{
    public override bool Execute()
    {
        FindObjectOfType<WorldLogic>().endGame();
        return true;
    }
}
