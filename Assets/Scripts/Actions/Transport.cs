using UnityEngine;
using UnityEngine.Apple;

public class Transport : Action
{
    public Transform destination;
    private AudioSource audioSource;

    void Start()
    {

        audioSource = GetComponent<AudioSource>();

    }
    
    public override bool Execute()
    {
        StartCoroutine(FindObjectOfType<Fader>().FadeOut());
        Player player = FindObjectOfType<Player>();
        player.gameObject.transform.position = destination.position;
        audioSource.Play();
        return true;
    }
}
