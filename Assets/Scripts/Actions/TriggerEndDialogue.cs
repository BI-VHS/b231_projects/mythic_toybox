using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEndDialogue : Action
{
    public GameObject dialogueToInvoke;
    public Action doAfter = null;
    public override bool Execute()
    {
        StartCoroutine(DelayExecute());
        return true;
    }

    public IEnumerator DelayExecute() 
    {
        yield return new WaitForSeconds(1f);
        Object o = Instantiate(dialogueToInvoke);

        if (doAfter != null)
            doAfter.Execute();
    }
}
