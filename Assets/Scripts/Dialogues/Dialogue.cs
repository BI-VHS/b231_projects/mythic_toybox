using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "ScriptableObjects/Dialogue", order = 1)]
public class Dialogue : ScriptableObject
{
    public Sprite portrait;
    public string[] lines;
    public string[] answers;
    public Dialogue[] answerLeadingTo;
    public Dialogue leadsTo;
    public string speaker;

    public Action actionToTrigger;

    public void TriggerAction()
    {
        if (actionToTrigger != null)
        {
            actionToTrigger.Execute();
        }
    }
}
