using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaySimple : MonoBehaviour
{
    public GameObject speechBubble;
    public string text;
    public bool repeat = false;
    public bool alreadySaid = false;
    private GameObject canvas;
    private SpeechBubble speechBubbleInstance;
    void Start()
    {
        canvas = GameObject.Find("CanvasMainUI");
    }

    void Update()
    {
        if (speechBubbleInstance)
            speechBubbleInstance.fixPosition = transform.position;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && (repeat || (!repeat && !alreadySaid) ))
        {
            alreadySaid = true;

            GameObject bubbleInstance = Instantiate(speechBubble, canvas.transform);
            speechBubbleInstance = bubbleInstance.GetComponent<SpeechBubble>();

            speechBubbleInstance.text = text;
            speechBubbleInstance.fixPosition = transform.position;

            speechBubbleInstance.doBubble();
        }
    }
}
