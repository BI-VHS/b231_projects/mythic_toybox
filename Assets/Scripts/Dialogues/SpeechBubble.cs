using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class SpeechBubble : MonoBehaviour
{
    public string text = "";
    public float scrollSpeed = 0.01f;
    public Vector3 fixPosition = new Vector3(39f, 0f, 0f);
    public float waitAfterFinish = 1f;
    public Text textField;
    private RectTransform rectTransform;

    void Start()
    {  
        rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        rectTransform.position = new Vector3(fixPosition.x, fixPosition.y + 1f, fixPosition.z);
    }

    public void doBubble() 
    {
        StartCoroutine(ScrollLoop());

    }

    private IEnumerator ScrollLoop()
    {
        textField.text = string.Empty;
        foreach (char c in text)
        {
            textField.text += c;
            yield return new WaitForSeconds(scrollSpeed);
        }

        yield return new WaitForSeconds(waitAfterFinish);
        Destroy(gameObject);
    }

}
