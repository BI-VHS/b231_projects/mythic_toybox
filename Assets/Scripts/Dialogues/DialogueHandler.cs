using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueHandler : MonoBehaviour
{
    public Text textField;
    public Text nametag;
    public Dialogue dialogue;
    public float scrollSpeed = .01f;

    private Player player;

    private bool pressedInteract = false;
    private bool answerChosen = false;

    public GameObject answer0;
    public GameObject answer1;
    public GameObject answer2;
    public GameObject answer3;

    public GameObject avatarSlot;

    private List<GameObject> answerButtons;

    void Start()
    {
        // singleton check
        if (FindObjectsOfType<DialogueHandler>().Length > 1) {
            Destroy(this.gameObject);
        }

        answerButtons = new List<GameObject>();
        if (answer0)
            answerButtons.Add(answer0);
        if(answer1)
            answerButtons.Add(answer1);
        if (answer2)
            answerButtons.Add(answer2);
        if (answer3)
            answerButtons.Add(answer3);

        foreach (GameObject answer in answerButtons)
            answer.SetActive(false);

        // forbid the player from moving during the dialogue
        player = FindObjectOfType<Player>();
        player.allowMovement = false;
        player.allowInteractions = false;
        
        textField.text = string.Empty;
        nametag.text = string.Empty;
        nametag.text = dialogue.speaker;

        StartCoroutine(ScrollLoop());
    }

    private void Update()
    {
        if (Input.GetButtonDown("Interact1"))
        {
            pressedInteract = true;
        }
        else 
        {
            pressedInteract = false;
        }
    }

    private IEnumerator ScrollLoop()
    {
        avatarSlot.GetComponent<Image>().sprite = dialogue.portrait;

        foreach (string s in dialogue.lines)
        {
            textField.text = string.Empty;
            foreach (char c in s) {
                textField.text += c;
                yield return new WaitForSeconds(scrollSpeed);
            }
            yield return new WaitUntil(() => pressedInteract);
        }

        for (int i = 0; i < dialogue.answers.Length; i++)
        {
            answerButtons[i].SetActive(true);
            answerButtons[i].GetComponentInChildren<Text>().text = dialogue.answers[i];
        }

        if(dialogue.actionToTrigger != null)
        {
            dialogue.TriggerAction();
        }

        if (dialogue.answers.Length > 0)
        {
            yield return new WaitUntil(() => answerChosen);
            answerChosen = false;
        }
        else if (dialogue.leadsTo != null) 
        {
            yield return new WaitUntil(() => pressedInteract);

            dialogue = dialogue.leadsTo;
            textField.text = string.Empty;
            nametag.text = dialogue.speaker;

            foreach (GameObject answer in answerButtons)
                answer.SetActive(false);

            StartCoroutine(ScrollLoop());
        }
        else
        {
            ExitDialogue();
        }
    }

    private void ExitDialogue() {
        player.allowMovement = true;
        player.allowInteractions = true;
        Destroy(this.gameObject);
    }

    public void triggerAnswer(int index)
    {
        answerChosen = true;
        if (index < dialogue.answerLeadingTo.Length && dialogue.answerLeadingTo[index])
        {
            dialogue = dialogue.answerLeadingTo[index];
            textField.text = string.Empty;
            nametag.text = dialogue.speaker;

            foreach (GameObject answer in answerButtons)
                answer.SetActive(false);

            StartCoroutine(ScrollLoop());
        }
        else
        {
            ExitDialogue();
        }
    }
}
