using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpriteSortManager : MonoBehaviour
{
    public SpriteRenderer[] sprites;
    private IEnumerable<SpriteRenderer> orderedArray;
    void Start()
    {
        sprites = FindObjectsOfType<SpriteRenderer>();
    }

    public void UpdateDrawOrder() 
    {
        orderedArray = sprites.OrderBy(sprite => {
            return sprite?.gameObject.transform.position.y;
        });

        int sortOrder = sprites.Length;
        foreach (SpriteRenderer sprite in orderedArray)
        {   
            if (sprite != null && sprite?.gameObject.tag != "DontUpdateRender")
                sprite.sortingOrder = sortOrder;
            sortOrder--;
        }
    }

    public void ReloadSprites() 
    {
        sprites = FindObjectsOfType<SpriteRenderer>();
    }

    void Update()
    {
        UpdateDrawOrder();
    }
}
