using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBubble : MonoBehaviour
{
    private RectTransform rectTransform;

    Player player;
    void Start()
    {
        player = FindObjectOfType<Player>();
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        rectTransform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1f, 0);
    }
}
