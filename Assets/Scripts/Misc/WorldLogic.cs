using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering.Universal;

public class WorldLogic : MonoBehaviour
{
    public bool isNight = false;
    public GameObject nightFilter;

    public bool prohibition = true;

    public Light2D globalLight;
    public Light2D schoolLight;
    public Light2D houseLight;
    public Light2D houseOtherLight;
    public Light2D groceryLight;
    public Light2D farmLight;

    public float dark = 0.25f;
    public float lightness = 1.0f;

    public GameObject endingScreen;

    public GameObject player;

    public GameObject questlist;

    public bool testEndgame = false;

    public bool buddyTriggerAlreadyActivated = false;

    private void Start()
    {
        nightFilter.SetActive(false);
        allDark();
    }

    private void Update()
    {   
        if (nightFilter != null)
        {
            if (isNight)
               // nightFilter?.SetActive(true);
               globalLight.intensity = dark;
            else
                //nightFilter?.SetActive(false);
                globalLight.intensity = lightness;
        }

    }

    public void allDark(){
        schoolLight.intensity = dark;
        farmLight.intensity = dark;
        houseLight.intensity = dark;
        houseOtherLight.intensity = dark;
        groceryLight.intensity = dark;
    }

    public void allLight(){
        schoolLight.intensity = lightness;
        farmLight.intensity = lightness;
        houseLight.intensity = lightness;
        houseOtherLight.intensity = lightness;
        groceryLight.intensity = lightness;
    }

    public void finishedAll(){
        allLight();
        globalLight.intensity = lightness;
    }

    public void finishedGrocery(){
        groceryLight.intensity = lightness;
    }

    public void finishedFarm(){
        farmLight.intensity = lightness;
    }

    public void finishedComrade(){
        houseLight.intensity = lightness;
    }
    public void finishedGrandma(){
        houseOtherLight.intensity = lightness;
    }

    public void finishedSchool(){
        schoolLight.intensity = lightness;
    }

    public void endGame(){
        FindObjectOfType<Fader>().FadeOut();
        endingScreen.SetActive(true);
        player.GetComponent<Player>().allowMovement = false;
        questlist.SetActive(false);
    }
}
