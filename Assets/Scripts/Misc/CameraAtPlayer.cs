using UnityEngine;

public class CameraAtPlayer : MonoBehaviour
{
    public float zoom = 5f;
    private Camera gameCamera;

    private Player player;
    void Start()
    {
        player = FindObjectOfType<Player>();
        gameCamera = this.gameObject.GetComponent<Camera>();
    }

    private void Update()
    {
        this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z - 10);
        gameCamera.orthographicSize = zoom;
    }

}
