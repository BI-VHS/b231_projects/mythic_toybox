using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextWhiteOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        this.gameObject.GetComponentInChildren<Text>().color = Color.white;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.gameObject.GetComponentInChildren<Text>().color = Color.black;

    }
}
