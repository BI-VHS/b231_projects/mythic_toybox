using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    // movement
    public float movSpeed = 3f;
    float vertical;
    float horizontal;
    Vector2 movement;
    public bool allowMovement = true;
    public bool allowInteractions = true;
    Rigidbody2D rb;

    // interaction
    public LayerMask interactablesLayer;
    public LayerMask drinkersLayer;
    public float interactionReach = 1.3f;

    // animation
    private Animator animator;

    // inventory
    public GameObject heldItemBubble;
    public enum Item 
    {
        water, beer, none, wood
    }
    public Item heldItem = Item.none;

    public Sprite waterSprite;
    public Sprite beerSprite;
    public Sprite woodSprite;
    public Image heldItemImage;

    public Action startQuest;

    public AudioSource walkSource;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        //audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleControls();
        HandleHeldItem();
        animator.SetFloat("xMovement", horizontal);
        animator.SetFloat("yMovement", vertical);
    }

    // Listens to input events and reacts to them
    void HandleControls()
    {
        vertical = Input.GetAxisRaw("Vertical");
        horizontal = Input.GetAxisRaw("Horizontal");
        movement = new Vector2(horizontal, vertical);

        if (Input.GetButtonDown("Interact1") && allowInteractions)
        {
            if(!audioSource.isPlaying) audioSource.Play();
            Collider2D[] detectedColliders = Physics2D.OverlapCircleAll(transform.position, interactionReach, interactablesLayer);
            if (detectedColliders.Length > 0)
            {
                detectedColliders[0].GetComponent<Interactable>().TriggerInteraction();
            }
            Collider2D[] detectedDrinkerColliders = Physics2D.OverlapCircleAll(transform.position, interactionReach, drinkersLayer);
            if (detectedDrinkerColliders.Length > 0 && heldItem != Item.none)
            {
                if(heldItem == Item.beer || heldItem == Item.water)
                    if (detectedDrinkerColliders[0].GetComponent<DeliverDrink>().wantsDrink) 
                    {
                        detectedDrinkerColliders[0].GetComponent<DeliverDrink>().GiveDrink();
                    }

                if(heldItem == Item.wood)
                    if (detectedDrinkerColliders[0].GetComponent<DeliverWood>().wantsWood) 
                    {
                        detectedDrinkerColliders[0].GetComponent<DeliverWood>().GiveWood();
                    }
            }
        }
    }

    // Updates player's position according to the controls
    void FixedUpdate()
    {
        if (allowMovement)
        {
            if(!walkSource.isPlaying && movement != new Vector2(0,0)) walkSource.Play();
            rb.velocity = Vector3.zero;
            rb.MovePosition(rb.position + movement * movSpeed * Time.fixedDeltaTime);
        }
    }

    private void OnDrawGizmosSelected()
    {

        Gizmos.DrawWireSphere(transform.position, interactionReach);
    }

    private void HandleHeldItem() 
    {
        switch (heldItem) 
        {
            case Item.none:
                heldItemBubble.SetActive(false);
                break;
            case Item.water:
                heldItemImage.sprite = waterSprite;
                heldItemBubble.SetActive(true);
                break;
            case Item.beer:
                heldItemImage.sprite = beerSprite;
                heldItemBubble.SetActive(true);
                break;
            case Item.wood:
                heldItemImage.sprite = woodSprite;
                heldItemBubble.SetActive(true);
                break;
        
        }
    }

}
