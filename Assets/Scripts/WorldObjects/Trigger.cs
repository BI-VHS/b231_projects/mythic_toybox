using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public Action action;
    public bool triggerEnabled = true;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(triggerEnabled && collision.name == "Player")
        {
            action.Execute();
        }
    }
}
