using System.Collections;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public Action interactionAction;
    public Action onClosedAction = null;
    public bool repeatable = true;
    public bool destroyOnInteract = false;

    public bool openAtNight = false;
    public bool openAtDay = false;

    public bool canInteract = true;

    public Player.Item requiredItem = Player.Item.none;
    public bool takeItemAway = false;

    private bool isNPC = false;

    private WorldLogic worldLogic;

    public AudioSource playSound=null;

    private void Start()
    {   
        worldLogic = FindObjectOfType<WorldLogic>();

        if (gameObject.GetComponent<BaseNPC>())
            isNPC = true;
    }

    public void TriggerInteraction()
    {
        bool rightTime = ((openAtDay && !worldLogic.isNight) || (openAtNight && worldLogic.isNight));

        if (canInteract && !rightTime && onClosedAction != null)
        {
            if (isNPC)
            {
                gameObject.GetComponent<BaseNPC>().allowMovement = false;
            }
            onClosedAction.Execute();
        }

        if (canInteract && rightTime)
        {
            if (playSound != null) playSound.Play();
            if (FindObjectOfType<Player>().heldItem != requiredItem && requiredItem != Player.Item.none)
                return;

            if (takeItemAway)
                FindObjectOfType<Player>().heldItem = Player.Item.none;

            if (isNPC)
            {
                gameObject.GetComponent<BaseNPC>().allowMovement = false;
            }

            interactionAction.Execute();

            if (!repeatable)
                canInteract = false;

            if (destroyOnInteract) 
            {
                gameObject.transform.position = new Vector3(300000000f, 300000000f, 300000000f);
                FindObjectOfType<SpriteSortManager>().ReloadSprites();
                FindObjectOfType<DrinkSpawner>().ResetSpawner();
                FindObjectOfType<WoodSpawner>().ResetSpawner();
            }
        }
    }
}
