using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseNPC : MonoBehaviour
{
    // movement
    public float movSpeed = 3f;
    float vertical;
    float horizontal;
    Vector2 movement;

    public bool loop = false;
    public List<Transform> waypoints;
    public List<float> waitTime;

    private Queue<Transform> waypointsToReach;
    private Queue<Transform> reachedWaypoints;
    private Transform currentWaypoint;

    public bool allowMovement = true;

    Rigidbody2D rb;

    private Animator animator;

    private SpriteSortManager spriteSortManager;
    private AudioSource audioSource;

    void Start()
    {
        if (waypoints.Count > 0)
        {
            waypointsToReach = new Queue<Transform>();
            reachedWaypoints = new Queue<Transform>();

            foreach (Transform t in waypoints)
            {
                waypointsToReach.Enqueue(t);
            }
        }

        spriteSortManager = FindObjectOfType<SpriteSortManager>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

    }

    void Update()
    {
        UpdateAnimator();
        movement = new Vector2(horizontal, vertical);

        if (!allowMovement)
            if (!FindObjectOfType<DialogueHandler>())
                allowMovement = true;

        StartCoroutine(HandleWaypoints());
    }

    void FixedUpdate()
    {
        if (allowMovement)
        {
            if(!audioSource.isPlaying && movement != new Vector2(0,0)) audioSource.Play();
            rb.velocity = Vector3.zero;
            rb.MovePosition(rb.position + movement * movSpeed * Time.fixedDeltaTime);
        }
    }

    private void UpdateAnimator()
    {
        if (allowMovement) 
        {
            animator.SetFloat("xMovement", horizontal);
            animator.SetFloat("yMovement", vertical);
        }
        else
        {
            animator.SetFloat("xMovement", 0);
            animator.SetFloat("yMovement", 0);
        }

    }

    private bool CheckResetQueues()
    {
        if (!loop)
            return false;

        if (waypointsToReach.Count < 1)
        {
            while (reachedWaypoints.Count > 0)
            {
                Transform t = reachedWaypoints.Dequeue();
                waypointsToReach.Enqueue(t);
            }
            return true;
        }
        return false;
    }

    private bool isCloseEnough()
    {
        if (currentWaypoint) {
            float tolerance = 0.1f;
            bool condX = Mathf.Abs(currentWaypoint.transform.position.x - gameObject.transform.position.x) < tolerance;
            bool condY = Mathf.Abs(currentWaypoint.transform.position.y - gameObject.transform.position.y) < tolerance;

            return condX && condY;
        }
        else
            return false;
    }

    private IEnumerator WaitOnWaypoint(int index)
    {
        if (waitTime.Count >= index + 1) 
        { 
            yield return new WaitForSeconds(waitTime[index]);
        }
        else
        {
            yield return null;

        }
    }

    int currentWaypointIndex = 0;


    bool handlingWaypoints = false;
    private IEnumerator HandleWaypoints() 
    {
        if (!handlingWaypoints)
        {
            handlingWaypoints = true;
            // check if reached, stop if reached
            if (isCloseEnough())
            {
                horizontal = 0;
                vertical = 0;
                reachedWaypoints.Enqueue(currentWaypoint);
                yield return StartCoroutine(WaitOnWaypoint(currentWaypointIndex));
                currentWaypoint = null;
            }

            // check queues and reset if needed
            if (!currentWaypoint && waypoints.Count > 0)
            {
                bool restarted = CheckResetQueues();
                currentWaypoint = waypointsToReach.Dequeue();

                if (restarted)
                    currentWaypointIndex = 0;
                else
                    currentWaypointIndex++;
            }

            // if standing, set motion
            if (currentWaypoint)
            {
                // calculate direction vector
                Vector2 normalizedMovement = new Vector2(currentWaypoint.transform.position.x - gameObject.transform.position.x, currentWaypoint.transform.position.y - gameObject.transform.position.y).normalized;
                horizontal = normalizedMovement.x;
                vertical = normalizedMovement.y;
            }
            handlingWaypoints = false;
        }
    }
}
