using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkSpawner : MonoBehaviour
{
    public GameObject beerPrefab;
    public GameObject waterPrefab;

    public Transform spawnPosition;

    public Player.Item itemToSpawn = Player.Item.water;

    private Object spawnedInstance = null;

    private bool spawningLock = false;

    private WorldLogic worldLogic;
    public AudioSource pourSound;

    private void Start()
    {
        worldLogic = FindObjectOfType<WorldLogic>();
    }

    private void Update()
    {
        itemToSpawn = (worldLogic.prohibition ? Player.Item.water : Player.Item.beer);

        if (spawnedInstance == null && !spawningLock) 
        {
            spawningLock = true;
            StartCoroutine(SpawnItemCoroutine());
        }
    }

    private IEnumerator SpawnItemCoroutine() 
    {
        yield return new WaitForSeconds(Random.Range(1f, 5f));
        spawnedInstance = Instantiate((itemToSpawn == Player.Item.water ? waterPrefab : beerPrefab), spawnPosition);
        spawningLock = false;
        pourSound.Play();
    }

    public void ResetSpawner() 
    {
        spawnedInstance = null;
    }
}
