using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverDrink : MonoBehaviour
{
    public bool wantsDrink = false;
    public AudioSource drinkSource;

    public void GiveDrink() 
    {
        gameObject.GetComponent<Animator>().SetTrigger("TriggerDrinking");
        FindObjectOfType<Player>().heldItem = Player.Item.none;
    }

    public void playSound(){
        drinkSource.Play();
    }
}
