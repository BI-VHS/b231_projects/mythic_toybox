using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerOrWater : MonoBehaviour
{
    public Sprite beerSprite;
    public Sprite waterSprite;

    private WorldLogic worldLogic;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        worldLogic = FindObjectOfType<WorldLogic>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        spriteRenderer.sprite = (worldLogic.prohibition ? waterSprite : beerSprite);
    }
}
